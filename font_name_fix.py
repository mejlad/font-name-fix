
import os
import unohelper
from time import sleep
from threading import Thread
from com.sun.star.document import XEventListener
from com.sun.star.beans import PropertyValue
from com.sun.star.frame import XDispatchProvider, XDispatch, FeatureStateEvent
from com.sun.star.lang import XInitialization


class DocumentEventListener(unohelper.Base, XEventListener):
    def __init__(self, doc):
        self.doc = doc
        self.vc = self.doc.CurrentController.ViewCursor
        self.fn = self.vc.CharFontName
        self.fnc = self.vc.CharFontNameComplex

    def notifyEvent(self, ev):
        if self.vc.CharFontName != self.fn or self.vc.CharFontNameComplex != self.fnc:
            if self.vc.CharFontName != self.fn:
                self.vc.CharFontNameComplex = self.fn = self.vc.CharFontName
            if self.vc.CharFontNameComplex != self.fnc:
                self.vc.CharFontName = self.fnc = self.vc.CharFontNameComplex
            return
        return


class FontNameFixHandler(unohelper.Base, XDispatchProvider, XInitialization):
    def __init__(self, ctx):
        self.ctx = ctx
        self.frame = None

    def initialize(self, objs):
        if len(objs) > 0:
            self.frame = objs[0]
        return

    def queryDispatch(self, url, target, searchflags):
        if url.Protocol == "FontNameFix:" and url.Path == "Menuitem":
            smgr = self.ctx.getServiceManager()
            dispatch = smgr.createInstanceWithArgumentsAndContext(
                "LibreOffice.ProtocolHandler.FontNameFixDispatch",
                (self.frame, ), self.ctx)
            return dispatch
        return None

    def queryDispatches(self, requests):
        result = []
        for item in requests:
            result.append(self.queryDispatch(item.FeatureURL, item.FrameName, item.SearchFlags))
        return tuple(result)


class FontNameFixDispatch(unohelper.Base, XDispatch):

    def __init__(self, ctx, args):
        self.ctx = ctx
        self.frame = args
        self.desktop = self.ctx.ServiceManager.createInstanceWithContext("com.sun.star.frame.Desktop", self.ctx)
        self.document_event_listener = None
        self.listener = None
        self.doc = None

    def after_loading(self, url):
        try:
            self.doc = self.document()
            self.document_event_listener = DocumentEventListener(self.doc)
            sleep(1)
            if self.state:
                self.doc.addEventListener(self.document_event_listener)
        except Exception as e:
            print(e)
        finally:
            self.sendCommandTo(self.listener, url, after_loading=True)

    def __new__(cls, ctx, args):
        obj = super().__new__(cls)
        status = {'0': False, '1': True}
        filename = os.path.join(os.path.dirname(__file__), 'settings.txt')
        with open(filename, "r") as settings:
            obj.state = status[settings.read().strip()]
        return obj

    def dispatch(self, url, args):
        self.state = not self.state
        try:
            if url.Protocol == 'FontNameFix:' and url.Path == "Menuitem":
                self.sendCommandTo(self.listener, url)
        except Exception as e:
            print(e)
        finally:
            return

    def addStatusListener(self, listener, url):
        if not self.listener:
            self.listener = listener
            t = Thread(target=self.after_loading, args=(url,))
            t.start()
            return
        return

    def removeStatusListener(self, listener, url):
        return

    def sendCommandTo(self, listener, url, enabled=True, after_loading=False):
        if not after_loading:
            if self.state:
                self.doc.addEventListener(self.document_event_listener)
            else:
                self.doc.removeEventListener(self.document_event_listener)
            self.set_state(self.state)
        event = FeatureStateEvent(self, url, "ItemChecked", enabled, False, self.state)
        self.listener.statusChanged(event)
        return

    def set_state(self, state):
        status = {True: 1, False: 0}
        filename = os.path.join(os.path.dirname(__file__), 'settings.txt')
        with open(filename, "w+") as settings:
            settings.write(f'{status[state]}')

    def document(self):
        controller = self.frame.Controller
        if controller:
            return controller.getModel()
        return self.desktop.getCurrentComponent()


g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(
    FontNameFixDispatch,
    "LibreOffice.ProtocolHandler.FontNameFixDispatch",
    ("com.sun.star.frame.XDispatch",), )
g_ImplementationHelper.addImplementation(
    FontNameFixHandler,
    "LibreOffice.ProtocolHandler.FontNameFixHandler",
    ("com.sun.star.frame.ProtocolHandler",), )
